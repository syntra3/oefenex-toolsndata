"""
Opdracht:

Bepalingen:
 - Je moet gebruik maken van de aangeleverde variable(n)
 - Je mag deze variable(n) niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Inlever datum is zondag avond 13 maart 2022 18:00CET
 - Als inlever formaat wordt een git url verwacht die gecloned kan worden

/ 5 ptn 1 - Maak een public repository aan op jouw gitlab account voor dit project
/10 ptn 2 - Gebruik python om de gegeven api url aan te spreken
/20 ptn 3 - Gebruik regex om de volgende data te extracten:
    - Jaar, maand en dag van de created date
    - De provider van de nameservers (bijv voor 'ns3.combell.net' is de provider 'combell')
/15 ptn 4 - Verzamel onderstaande data en output alles als yaml. Een voorbeeld vind je hieronder.
    - Het land van het domein
    - Het ip van het domain
    - De DNS provider van het domein
    - Aparte jaar, maand en dag van de created date


Totaal  /50ptn
"""

""" voorbeeld yaml output

created:
  dag: '18'
  jaar: '2022'
  maand: '02'
ip: 185.162.31.124
land: BE
provider: combell

"""


import requests
import re
import yaml
from pprint import pprint

url = "https://api.domainsdb.info/v1/domains/search?domain=syntra.be"

response = requests.get(url).json()


date = response["domains"][0]['create_date']
regex_date = r"(?P<jaar>\d+)-(?P<maand>\d+)-(?P<dag>\d+)T"

for match in re.finditer(regex_date, date):
    datum_dict = match.groupdict()



ns_1 = response["domains"][0]['NS'][2]
regex_ns = r"\.(?P<ns_provider>.*)\."

for match in re.finditer(regex_ns, ns_1):
    ns_dict = match.groupdict()



output = {
"created": datum_dict,
"ip": response["domains"][0]['A'],
"land": response["domains"][0]['country'],
"provider": ns_dict["ns_provider"]
}

print(yaml.dump(output))